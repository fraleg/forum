package devcastzone.javaee.forum.utils;

import devcastzone.javaee.forum.dao.UzytkownikDAO;
import devcastzone.javaee.forum.encje.Uzytkownik;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class FiltrZalogowany implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
        req.setCharacterEncoding("UTF-8");
        String login = req.getRemoteUser();
        if (login != null) {
            Uzytkownik u = (Uzytkownik)req.getSession().getAttribute("uzytkownik");
            if (u == null) {
                UzytkownikDAO dao = (UzytkownikDAO)req.getAttribute("uzytkownicyDAO");
                u = dao.pobierzPoLoginie(login);
                req.getSession().setAttribute("uzytkownik", u);
            }
        }
        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
