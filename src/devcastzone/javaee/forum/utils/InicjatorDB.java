package devcastzone.javaee.forum.utils;


import devcastzone.javaee.forum.dao.TematyDAO;
import devcastzone.javaee.forum.dao.UzytkownikDAO;
import devcastzone.javaee.forum.dao.WpisyDAO;

import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class InicjatorDB implements ServletRequestListener {


    @Override
    public void requestDestroyed(ServletRequestEvent arg0) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent arg0) {
        EntityManager em = DBConfig.createEntityManager();
        UzytkownikDAO uzytkownikDAO = new UzytkownikDAO(em);
        TematyDAO tematyDAO = new TematyDAO(em);
        WpisyDAO wpisyDAO = new WpisyDAO(em);
        ServletRequest req = arg0.getServletRequest();
        req.setAttribute("wpisyDAO", wpisyDAO);
        req.setAttribute("tematyDAO",tematyDAO);
        req.setAttribute("uzytkownikDAO",uzytkownikDAO);
    }
}
