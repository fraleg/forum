package devcastzone.javaee.forum.encje;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="rola")
public class Rola implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    private String login;
    private String rola;

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getRola() {
        return rola;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRola(String rola) {
        this.rola = rola;
    }
}
